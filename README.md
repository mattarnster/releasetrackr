# releasetrackr
A GitHub API enabled release notifications tracker written in Go.

A live instance is running [here](https://releasetrackr.mattarnster.co.uk).

![Screenshot of releasetrackr](https://raw.githubusercontent.com/mattarnster/releasetrackr/master/releasetrackr.png)
